import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseApp } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the PurchasesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-purchases',
  templateUrl: 'purchases.html',
})
export class PurchasesPage {
  email1:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private firebase:FirebaseApp,private afAuth:AngularFireAuth) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PurchasesPage');
    this.loadDetails();
  }

  toHelpPage() {
    this.navCtrl.push('HelpPage');
  }

  loadDetails(){
    this.email1 = this.afAuth.auth.currentUser.email;  
  }

}
