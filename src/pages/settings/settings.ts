import { AngularFireAuth } from 'angularfire2/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PaymentInfoPage } from '../payment-info/payment-info';
import { RecentReceiptsPage } from '../recent-receipts/recent-receipts';
import { FirebaseApp } from 'angularfire2';


@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
 email:any;

  // public users: any[] = [
  //   { name: "Suhana Hamzah"}]

  constructor(public navCtrl: NavController, public navParams: NavParams, private afAuth:AngularFireAuth, private firebase:FirebaseApp) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
    this.loadDetails();
  }

  public presentUserInfoAlert(user: any): void {
    console.log(JSON.stringify(user));
  }

  tab1Root = PaymentInfoPage;
  tab2Root = RecentReceiptsPage;

  loadDetails(){
    this.email = this.afAuth.auth.currentUser.email;  
  }

}