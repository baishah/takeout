import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RecentReceiptsPage } from '../recent-receipts/recent-receipts';
import { PurchasesPage } from '../purchases/purchases';


@IonicPage()
@Component({
  selector: 'page-view-receipts',
  templateUrl: 'view-receipts.html',
})
export class ViewReceiptsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewReceiptsPage');
  }

  viewReceipts() {
    this.navCtrl.push(PurchasesPage);
  }
}
