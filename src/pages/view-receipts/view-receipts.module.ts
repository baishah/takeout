import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewReceiptsPage } from './view-receipts';

@NgModule({
  declarations: [
    ViewReceiptsPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewReceiptsPage),
  ],
})
export class ViewReceiptsPageModule {}
