import { AddCardPage } from './../add-card/add-card';
import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Nav } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-payment-info',
  templateUrl: 'payment-info.html',
})
export class PaymentInfoPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public nav:Nav) {}
  
  addPayment() {
    this.nav.setRoot(AddCardPage);
   }
}

