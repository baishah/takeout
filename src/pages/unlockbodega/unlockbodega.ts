import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/*
  Modified by Siti Aishah 130818
*/

@IonicPage()
@Component({
  selector: 'page-unlockbodega',
  templateUrl: 'unlockbodega.html',
})
export class UnlockbodegaPage {

  // @ViewChild("numberInput") numberInput;
//   public setInputFocus(){
//     var elem:any = this.numberInput;
//     elem._native.nativeElement.focus(); // Keep the focus on input field.
// }

  masks:any;

  num1:any = "";
  num2:any = "";
  num3:any = "";

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.masks = {
      num1:[/\d/,' ',/\d/,' ',/\d/],
    
    };
  }

  save(){
    let unmaskeddata = {
      num1: this.num1.replace(/\D+/g, ''),
     
    };
    console.log(unmaskeddata);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UnlockbodegaPage');
    //this.keyboard.show(); // Needed for android. Call in a platform ready function
    //this.numberInput.setFocus();
  }

}
