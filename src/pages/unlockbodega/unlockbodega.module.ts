import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UnlockbodegaPage } from './unlockbodega';

@NgModule({
  declarations: [
    UnlockbodegaPage,
  ],
  imports: [
    IonicPageModule.forChild(UnlockbodegaPage),
  ],
})
export class UnlockbodegaPageModule {}
