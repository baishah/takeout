import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { SignupPage } from '../signup/signup';
import { AuthProvider } from './../../providers/auth/auth';
import { usercreds } from '../../models/user'; 
import { UnlockbodegaPage } from '../unlockbodega/unlockbodega';
import { PasswordresetPage } from '../passwordreset/passwordreset';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {

  credentials = {} as usercreds;

  constructor(private afAuth: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams, public authservice : AuthProvider, public alertCtrl:AlertController, public loadingCtrl:LoadingController) {
  }
 
  signin() {
    this.authservice.login(this.credentials).then((res:any)=>{
      if(!res.code)
        this.navCtrl.setRoot(UnlockbodegaPage);
      else
        alert(res);
    })
  }

  signup(){
    this.navCtrl.push(SignupPage)
  }

  passwordreset(){
    this.navCtrl.push(PasswordresetPage);
  }
}