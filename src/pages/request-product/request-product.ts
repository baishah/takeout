import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { RequestProvider } from '../../providers/request/request';
import { UnlockbodegaPage } from '../unlockbodega/unlockbodega';

/*
  Created by Siti Aishah 140818
*/

@IonicPage()
@Component({
  selector: 'page-request-product',
  templateUrl: 'request-product.html',
})
export class RequestProductPage {

  newproduct = {
    product:''
  }
  constructor(public request:RequestProvider,public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {}

  submit() {
    let loader = this.loadingCtrl.create({
      content:'please wait'
    })
    loader.present();
    this.request.newproduct(this.newproduct).then((res:any)=>{
      loader.dismiss();
      if(res.success){
      }else{
        this.navCtrl.setRoot(UnlockbodegaPage);
      }
    })
  }

}
