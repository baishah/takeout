import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserProvider } from '../../providers/user/user';
import { UnlockbodegaPage } from '../unlockbodega/unlockbodega';
import { SigninPage } from '../signin/signin';

/*
  Created by Siti Aishah 140818
*/

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
newuser = {
  email: '',
  password:''
}

  constructor(public userservice:UserProvider,private afAuth: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
  }

  onFbLogin() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then((res) => console.log(res));
  }

  signup() {
   var toaster = this.toastCtrl.create({
     duration:3000,
     position:'bottom'
   });
   if(this.newuser.email == '' || this.newuser.password == ''){
     toaster.setMessage('All fields are required dude');
     toaster.present();
   }
   else if(this.newuser.password.length < 8){
     toaster.setMessage('password is not strong.Try giving more than 7 character');
     toaster.present();
   }
   else{
     let loader = this.loadingCtrl.create({
       content:'Please wait'
     });
     loader.present();
     this.userservice.adduser(this.newuser).then((res:any)=>{
       loader.dismiss();
       if (res.success)
          this.navCtrl.push(UnlockbodegaPage);
      else
        alert('Error' + res);
     })
   }
  }

 signin(){
   this.navCtrl.setRoot(SigninPage);
 }
  
}
