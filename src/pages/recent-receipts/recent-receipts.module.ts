import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecentReceiptsPage } from './recent-receipts';

@NgModule({
  declarations: [
    RecentReceiptsPage,
  ],
  imports: [
    IonicPageModule.forChild(RecentReceiptsPage),
  ],
})
export class RecentReceiptsPageModule {}
