import { HelpPage } from './../help/help';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseApp } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { App } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-recent-receipts',
  templateUrl: 'recent-receipts.html',
})
export class RecentReceiptsPage {
  email1:any;
  
  constructor(private app: App, public navCtrl: NavController, public navParams: NavParams,private firebase:FirebaseApp,private afAuth:AngularFireAuth) {
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecentReceiptsPage');
    this.loadDetails();
    
  }

  toHelpPage() {
    
    //this.navCtrl.setRoot(HelpPage);
    
    let nav = this.app.getRootNav();
    nav.push(HelpPage);
  }
  

  loadDetails(){
    this.email1 = this.afAuth.auth.currentUser.email;  
  }

}
