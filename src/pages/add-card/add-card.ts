import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { CardProvider } from '../../providers/card/card';
import { UnlockbodegaPage } from '../unlockbodega/unlockbodega';
//import { Content } from 'ionic-angular';



/*
  modified by Siti Aishah 130818
*/

@IonicPage()
@Component({
  selector: 'page-add-card',
  templateUrl: 'add-card.html',
})

export class AddCardPage {
  //@ViewChild(Content) content: Content;
 

  masks:any;
  card = {
  cardNumber:'',
  cardExpiry:'',
  cvv:''
}

  constructor(public cardservice:CardProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.masks ={
      cardNumber:[/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
      cardExpiry:[/[0-1]/, /\d/, '/', /[1-2]/, /\d/],
      cvv:[/\d/, /\d/, /\d/]
    };

  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCardPage');
  }

  save()
  {
    let unmaskedData = {   
        cardNumber: this.card.cardNumber.replace(/\D+/g, ''),
        cardExpiry: this.card.cardExpiry.replace(/\D+/g, ''),
        cvv: this.card.cvv.replace(/\D+/g, '')
    };
    console.log(unmaskedData);
    this.cardservice.updateCard(unmaskedData);
    this.navCtrl.setRoot(UnlockbodegaPage);
  }

  // fixScroll(){
  //   if(this.device.platform == "Android"){
  //     setTimeout(() => {
  //       let element = document.getElementById("absence-textarea");
  //       let box = element.getBoundingClientRect();
  //       let top = Math.round(box.top*10);
  //       this.content.scrollTo(0, top, 100);
  //     }, 350);
  //   }
  // }
  // myfunction(){
  //   this.content.scrollTo(0,200);
  //   }
    
}
