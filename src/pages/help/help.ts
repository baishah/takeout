import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { HelpProvider } from '../../providers/help/help';
import { ImagehandlerProvider } from '../../providers/imagehandler/imagehandler';
import { UnlockbodegaPage } from '../unlockbodega/unlockbodega';

/* 
  Created by Siti Aishah 140818
*/

@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {
 
   newreport = {
     name:'',
     title:'',
     body:'',
     attachment:'https://image.ibb.co/nuSmbd/square_img.png'
   }
  constructor(public imgservice:ImagehandlerProvider,public helpservice:HelpProvider, public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams,public loadingCtrl:LoadingController) {}

  attachment(){
    if(this.newreport.title == ''){
      let namealert = this.alertCtrl.create({
        buttons:['okay'],
        message:'Please enter the title first. Thanks'
      });
      namealert.present();
    }
    else{
      let loader = this.loadingCtrl.create({
        content:'Loading, please wait...'
      });
      loader.present();
      this.imgservice.uploadpic(this.newreport.title).then((res:any)=>{
        loader.dismiss();
        if(res)
        this.newreport.attachment = res;
      }).catch((err)=>{
        alert(err);
      })
    }
  }

  submit(){
    this.helpservice.addnewreport(this.newreport).then(()=>{
      this.navCtrl.setRoot(UnlockbodegaPage);
    }).catch((err)=>{
      alert(JSON.stringify(err))
    })
  }
 
}