import { AngularFireAuth } from 'angularfire2/auth';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { UnlockbodegaPage } from '../pages/unlockbodega/unlockbodega';
import { ViewReceiptsPage } from '../pages/view-receipts/view-receipts';
import { RequestProductPage } from '../pages/request-product/request-product';
import { SettingsPage } from '../pages/settings/settings';
import { HelpPage } from '../pages/help/help';
import { FirebaseApp } from 'angularfire2'
import { SigninPage } from '../pages/signin/signin';
import { AuthProvider } from "../providers/auth/auth";
import firebase from 'firebase';

/*
  modified by Siti Aishah 130818
*/

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  
  email: any;
  user: any;
  rootpage:any = UnlockbodegaPage;
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SigninPage;

  pages: Array<{icon: string, title: string, component: any,}>;

  constructor( private firebase: FirebaseApp, public platform: Platform, public authprovider: AuthProvider, public statusBar: StatusBar, public splashScreen: SplashScreen, private afAuth:AngularFireAuth) {

    window.addEventListener("keyboardDidShow", () => {
      document.activeElement.scrollIntoView(false);
  });

    // this.config.set( 'scrollPadding', false )
		// this.config.set( 'scrollAssist', false )
		// this.config.set( 'autoFocusAssist', false )

    afAuth.authState.subscribe(auth => {
       if(!auth){
         this.rootpage = SigninPage;
       }
       else{
        this.home();      
       }
    });
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'unlock', title: 'Unlock a TAKEOUT', component: UnlockbodegaPage },
      { icon: 'list-box', title: 'Purchases', component: ViewReceiptsPage },
      { icon: 'add-circle', title: 'Request Products', component: RequestProductPage },
      { icon: 'settings', title: 'Settings', component: SettingsPage },
      { icon: 'ios-help-circle', title: 'Help', component: HelpPage },
      { icon: 'exit', title: 'Sign Out', component: null }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      });
  }

  loadDetails(){
    this.email = this.afAuth.auth.currentUser.email;  
  }

  openPage(page)  {
    switch (true) {

      case ((page.title == 'Sign Out')): {
        console.log('Clicked Logout button');
        this.logout(); 
      }
          break;

      default: {
        this.nav.setRoot(page.component);
      }
          break;
  }
}

  logout(){
    firebase.auth().signOut().then(() => {
      this.nav.setRoot(SigninPage);
    })
  }

  home(){
    this.loadDetails();
    this.nav.setRoot(UnlockbodegaPage);
  }


}