import { TextAvatarDirective } from './../directives/text-avatar/text-avatar';
import { WelcomePage } from './../pages/welcome/welcome';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { MyApp } from './app.component';
import { MainPage } from '../pages/main/main';
import { SignupPage } from '../pages/signup/signup';
import { SigninPage } from '../pages/signin/signin';
import { UnlockbodegaPage } from '../pages/unlockbodega/unlockbodega';
import { PurchasesPage } from '../pages/purchases/purchases';
import { RequestProductPage } from '../pages/request-product/request-product';
import { SettingsPage } from '../pages/settings/settings';
import { HelpPage } from '../pages/help/help';
import { PaymentInfoPage } from '../pages/payment-info/payment-info';
import { RecentReceiptsPage } from '../pages/recent-receipts/recent-receipts';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AuthProvider } from '../providers/auth/auth';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { AddCardPage } from '../pages/add-card/add-card';
import { ViewReceiptsPage } from '../pages/view-receipts/view-receipts';
import { PurchasesPageModule } from '../pages/purchases/purchases.module';
import { HelpPageModule } from '../pages/help/help.module';
import { InventoryPage } from '../pages/inventory/inventory';
import { TextMaskModule } from 'angular2-text-mask';
import { UserProvider } from '../providers/user/user';
import { HelpProvider } from '../providers/help/help';
import { PasswordresetPage } from '../pages/passwordreset/passwordreset';
import { RequestProvider } from '../providers/request/request';
import { ImagehandlerProvider } from '../providers/imagehandler/imagehandler';
import { CardProvider } from '../providers/card/card';
import { FileChooser } from '@ionic-native/file-chooser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AnimationService, AnimatesDirective } from 'css-animator';



 var config = {
  apiKey: "AIzaSyBXxw8s_EEbt07NSB4SAOsvsC7zLcF9_hU",
    authDomain: "takeout-12807.firebaseapp.com",
    databaseURL: "https://takeout-12807.firebaseio.com",
    projectId: "takeout-12807",
    storageBucket: "takeout-12807.appspot.com",
    messagingSenderId: "568951663124"
  };
  
@NgModule({
  declarations: [
    MyApp,
    MainPage,
    SignupPage,
    SigninPage,
    UnlockbodegaPage,
    RequestProductPage,
    SettingsPage,
    PaymentInfoPage,
    RecentReceiptsPage,
    WelcomePage,
    TextAvatarDirective,
    AddCardPage,                                          
    ViewReceiptsPage,
    InventoryPage,
    PasswordresetPage,
    AnimatesDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp, {
      
      scrollAssist: false,
      //scrollDisable: false,
      autoFocusAssist: false,

    }),
    IonicStorageModule.forRoot(),
    AngularFireAuthModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    HttpClientModule,
    PurchasesPageModule,
    HelpPageModule,
    TextMaskModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MainPage,
    SignupPage,
    SigninPage,
    UnlockbodegaPage,
    PurchasesPage,
    RequestProductPage,
    SettingsPage,
    HelpPage,
    PaymentInfoPage,
    RecentReceiptsPage,
    WelcomePage,
    AddCardPage,
    ViewReceiptsPage,
    InventoryPage,
    PasswordresetPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    Camera,
    UserProvider,
    HelpProvider,
    HelpProvider,
    RequestProvider,
    ImagehandlerProvider,
    CardProvider,
    FileChooser,
    AnimationService
    
  ]
})
export class AppModule {}
