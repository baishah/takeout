import { Injectable } from '@angular/core';
import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import firebase from 'firebase'

/*
  Created by Siti Aishah 140818
*/

@Injectable()
export class ImagehandlerProvider {

  nativepath:any;
  fireimage = firebase.storage();

  constructor(public filechooser: FileChooser) {
    console.log('Hello ImagehandlerProvider Provider');
  }

  uploadpic(title){
    var promise = new Promise((resolve, reject) => {
      this.filechooser.open().then((url) => {
        (<any>window).FilePath.resolveNativePath(url, (result) => {
          this.nativepath = result;
          (<any>window).resolveLocalFileSystemURL(this.nativepath, (res) => {
            res.file((resFile) => {
              var reader = new FileReader();
              reader.readAsArrayBuffer(resFile);
              reader.onloadend = (evt: any) => {
                var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                var imageStore = this.fireimage.ref('/images').child(firebase.auth().currentUser.uid).child(title);
                imageStore.put(imgBlob).then((res) => {
                  this.fireimage.ref('/images').child(firebase.auth().currentUser.uid).child(title).getDownloadURL().then((url) => {
                    resolve(url);
                  }).catch((err) => {
                      reject(err);
                  })
                }).catch((err) => {
                  reject(err);
                })
              }
            })
          })
        })
    })
  })    
   return promise;   
  }
}
