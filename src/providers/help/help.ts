import { Injectable } from '@angular/core';
import  firebase from 'firebase';
import { Events } from 'ionic-angular';

/*
  Created by Siti Aishah 140818
*/

@Injectable()
export class HelpProvider {

  firehelp = firebase.database().ref('/help');

  constructor(public events:Events) {}

  addnewreport(newreport){
    var promise = new Promise((resolve,reject)=>{
      this.firehelp.child(firebase.auth().currentUser.uid).push().set({
        name: newreport.name,
        title: newreport.title,
        body: newreport.body,
        attachment:newreport.attachment
      }).then(()=>{
        resolve(true);
      }).catch((err)=>{
        reject(err);
      })
    });
    return promise;
  }

}

