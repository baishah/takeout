import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { usercreds } from '../../models/user';

@Injectable()
export class AuthProvider {

  constructor(public afAuth: AngularFireAuth) {}
  
  login(credentials:usercreds){
    var promise = new Promise((resolve,reject)=>{
      this.afAuth.auth.signInWithEmailAndPassword(credentials.email,credentials.password).then(()=>{
        resolve(true);
      }).catch((err)=>{
        reject(err);
      })
    })
    return promise;
  }
}
