import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { Events } from 'ionic-angular';

/*
  Created by Siti Aishah 140818
*/

@Injectable()
export class CardProvider {

  firecard = firebase.database().ref('/card');

  constructor(public afdb:AngularFireDatabase, public events:Events) {}

  updateCard(card){
    let path = this.firecard.child(firebase.auth().currentUser.uid)

    this.firecard.child(firebase.auth().currentUser.uid).once('value',(snapshot)=>{
      if(snapshot.val()===null){
        this.afdb.object(path).set({
          cardnumber:card.cardNumber,
          expirydate:card.cardExpiry,
          cvv:card.cvv
        });
      }else{
        this.afdb.object(path).update({
          cardnumber:card.cardNumber,
          expirydate:card.cardExpiry,
          cvv:card.cvv
        });
      }
    })
  }

}
