import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { Events } from 'ionic-angular';

/*
 Created by Siti Aishah 140818
*/

@Injectable()
export class RequestProvider {

  firerequest = firebase.database().ref('/product request')
  constructor() {
   }
  
   newproduct(prod){
     var promise = new Promise((resolve,reject)=>{
       this.firerequest.child(firebase.auth().currentUser.uid).push().set({
         product:prod.product
       }).then(()=>{
         resolve(true);
       }).catch((err)=>{
         reject(err);
       })
     });
     return promise;
   }
}
