import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import firebase from 'firebase';

/*
  Created by Siti Aishah 140818
*/

@Injectable()
export class UserProvider {
  firedata = firebase.database().ref('/users');
  firecard = firebase.database().ref('/card details')
  

  constructor(public afireauth: AngularFireAuth) {}

  adduser(newuser){
    var promise = new Promise((resolve,reject)=>{
      this.afireauth.auth.createUserWithEmailAndPassword(newuser.email,newuser.password).then(()=>{
        this.afireauth.auth.currentUser.updateProfile({
          displayName:newuser.email,
          photoURL: 'https://firebasestorage.googleapis.com/v0/b/myapp-4eadd.appspot.com/o/chatterplace.png?alt=media&token=e51fa887-bfc6-48ff-87c6-e2c61976534e'
        }).then(()=>{
          this.firedata.child(this.afireauth.auth.currentUser.uid).set({
            uid:this.afireauth.auth.currentUser.uid,
            displayName:newuser.email,
            photoURL: 'https://firebasestorage.googleapis.com/v0/b/myapp-4eadd.appspot.com/o/chatterplace.png?alt=media&token=e51fa887-bfc6-48ff-87c6-e2c61976534e'
          }).then(()=>{
            resolve({success:true});
          }).catch((err)=>{
            reject(err);
          })
        }).catch((err)=>{
          reject(err);
        })
      }).catch((err)=>{
        reject(err);
      })
    })
    return promise;
  }

  passwordreset(email){
    var promise = new Promise((resolve, reject)=>{
      firebase.auth().sendPasswordResetEmail(email).then(()=>{
        resolve({success:true});
      }).catch((err)=>{
        reject(err);
      })
    })
    return promise;
  }

  getuserdetails() {
    var promise = new Promise((resolve, reject) => {
    this.firedata.child(firebase.auth().currentUser.uid).once('value', (snapshot) => {
      resolve(snapshot.val());
    }).catch((err) => {
      reject(err);
      })
    })
    return promise;
  }

  addcard(card){
    var promise = new Promise((resolve,reject)=>{
      this.firedata.child(firebase.auth().currentUser.uid).set({
        cardNumber: card.cardNumber,
        expiryDate: card.expiryDate,
        cvv: card.cvv,
        }).then(()=>{
        resolve(true);
      }).catch((err)=>{
        reject(err);
      })
    });
    return promise;
  }

}
