import { ColorGenerator } from './color-generator';
import { Directive,ElementRef, Input, SimpleChanges } from '@angular/core';

/**
 * Generated class for the TextAvatarDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: 'text-avatar', // Attribute selector
  providers:[ColorGenerator]
})
export class TextAvatarDirective {

  constructor(private element: ElementRef, private colorGenerator: ColorGenerator) {}

  @Input() text:string;
  @Input() color:string;

  ngOnChanges(changes:SimpleChanges){
    let text = changes['text']? changes['text'].currentValue:null;
    let color = changes['color']? changes['color'].currentValue:null;

    this.element.nativeElement.setAttribute("value", this.ExtractFirstCharacter(text));
    this.element.nativeElement.style.backgroundColor = this.backgroundColorHexString(color,text);
  }

  private ExtractFirstCharacter(text:string) :string {
    return text.charAt(0) || '';
  }

  private backgroundColorHexString(color:string,text:string):string{
    return color || this.colorGenerator.getColor(this.ExtractFirstCharacter(text));
  }
}
